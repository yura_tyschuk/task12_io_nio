package compare.perfomance;

import java.io.IOException;

public class Program {

  public static void main(String[] args) throws IOException {
    //CompareClass compareClass = new CompareClass();
    compareClass.usualReader();
    //compareClass.bufferedReader();
    int bufferSize = 1 * 1024 * 1024;
    compareClass.bufferedReaderWithDifferentSizes(bufferSize);
  }
}
