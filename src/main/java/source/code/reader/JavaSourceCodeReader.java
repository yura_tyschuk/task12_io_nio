package source.code.reader;

import java.io.BufferedReader;
import java.io.FileReader;

public class JavaSourceCodeReader {

  private String directory;
  private String line;
  private String input;

  public JavaSourceCodeReader(String directory) {
    this.directory = directory;
    this.input = "";
  }

  public void readContentFromFile() {
    try {
      BufferedReader file = new BufferedReader(new FileReader(directory));
      while ((line = file.readLine()) != null) {
        if (line.contains("//")) {
          input += line + '\n';
        }
      }

      System.out.print(input);

    } catch (Exception e) {
      System.out.print("Problem reading the file.");
    }
  }
}

