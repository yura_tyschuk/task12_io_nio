import java.io.IOException;
import menu.MyView;

public class Program {

  public static void main(String[] args) throws IOException {
    new MyView().show();

  }
}
