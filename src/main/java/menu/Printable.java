package menu;

import java.io.IOException;

public interface Printable {

  void print() throws IOException, InterruptedException;
}