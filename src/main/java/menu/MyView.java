package menu;

import client.server.Server;
import controller.Controller;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import serialize.ShipWithDroids;

public class MyView {

  private Controller controller;
  private Map<String, String> menu;
  private Map<String, Printable> methodsMenu;
  private static Scanner input = new Scanner(System.in);
  private static Logger logger = LogManager.getLogger();

  public MyView() throws IOException {
    controller = new Controller();
    menu = new LinkedHashMap<>();
    methodsMenu = new LinkedHashMap<>();
    menu.put("1", "1 - Serialize data");
    menu.put("2", "2 - Directory content");
    menu.put("3", "3 - Java source code");
    menu.put("4", "4 - Compare perfomance");
    menu.put("Q", "Quit");

    methodsMenu.put("1", this::serializeData);
    methodsMenu.put("2", this::directoryContent);
    methodsMenu.put("3", this::javaSourceCodeFileReader);
    methodsMenu.put("4", this::comparePerfomance);
  }

  private void outputMenu() {
    System.out.println("\n Menu: ");
    for (String str : menu.values()) {
      System.out.println(str);
    }
  }

  private void comparePerfomance() throws IOException {
    controller.usualReader();
    controller.bufferedReader();
    logger.warn("Print size of buffer: ");
    int bufferSize = input.nextInt();
    controller.bufferedReaderWithSize(bufferSize);
  }

  private void serializeData() {
    ShipWithDroids shipWithDroids;
    shipWithDroids = controller.setShipWithDroids();
    try {
      FileOutputStream fileOut = new FileOutputStream("shipWithDroids.ser");
      ObjectOutputStream out = new ObjectOutputStream(fileOut);
      out.writeObject(shipWithDroids);
      out.close();
      fileOut.close();
      logger.fatal("Serialized data is saved");
    } catch (IOException e) {
      e.printStackTrace();
    }

    ShipWithDroids shipWithDroidsTMP;
    try {
      FileInputStream fileIn = new FileInputStream("shipWithDroids.ser");
      ObjectInputStream in = new ObjectInputStream(fileIn);
      shipWithDroidsTMP = (ShipWithDroids) in.readObject();
      in.close();
    } catch (IOException e) {
      e.printStackTrace();
      return;
    } catch (ClassNotFoundException e) {
      logger.warn("Ship with droids class not found");
      e.printStackTrace();
      return;
    }
    logger.fatal("Deserialized data: ");
    shipWithDroidsTMP.display();
  }


  private void directoryContent() {
    controller.getDirectoryContent();
  }

  private void javaSourceCodeFileReader() {
    controller.readContentFromFile();
  }

  public void show() {
    String keyMenu;
    do {
      outputMenu();
      System.out.println("Please select menu point: ");
      keyMenu = input.nextLine().toUpperCase();
      try {
        methodsMenu.get(keyMenu).print();

      } catch (Exception e) {

      }
    } while (!keyMenu.equals("Q"));
  }


}
