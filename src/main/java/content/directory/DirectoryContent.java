package content.directory;

import java.io.File;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class DirectoryContent {

  private static Logger logger = LogManager.getLogger();
  private static String directory;

  public DirectoryContent(String directory) {
    this.directory = directory;
  }

  public void getDirectoryContent() {
    logger.warn("Content of" + directory + " directory: ");
    File file = new File(directory);
    if (file.exists()) {
      printDirectoryContent(file);
    } else {
      logger.warn("Directory do not exists");
    }

  }

  private void printDirectoryContent(File file) {
    File[] filesList = file.listFiles();
    for (File f : filesList) {
      if (f.isDirectory()) {
        printDirectoryContent(f);
      }
      if (f.isFile()) {
        System.out.println("File: " + f.getName());
      }
    }
  }
}
