package controller;

import compare.perfomance.CompareClass;
import content.directory.DirectoryContent;
import java.io.IOException;
import serialize.ShipWithDroids;
import source.code.reader.JavaSourceCodeReader;

public class Controller {

  CompareClass compareClass;
  DirectoryContent directoryContent;
  ShipWithDroids shipWithDroids;
  JavaSourceCodeReader javaSourceCodeReader;

  public Controller() {
    compareClass = new CompareClass();
    directoryContent = new DirectoryContent("C:\\Users\\пк\\Desktop\\Навчання");
    shipWithDroids = new ShipWithDroids();
    javaSourceCodeReader = new JavaSourceCodeReader("Program.java");
  }


  public void usualReader() throws IOException {
    compareClass.usualReader();
  }

  public void bufferedReader() throws IOException {
    compareClass.bufferedReader();
  }

  public void bufferedReaderWithSize(int bufferSize) throws IOException {
    compareClass.bufferedReaderWithDifferentSizes(bufferSize);
  }

  public ShipWithDroids setShipWithDroids() {
    shipWithDroids.setNameOfDroids("Mark");
    shipWithDroids.setModelOfDroids("TM-15");
    shipWithDroids.setNumberOfDroids(15);
    shipWithDroids.setDamageOfDroids(25);
    return shipWithDroids;
  }

  public void readContentFromFile() {
    javaSourceCodeReader.readContentFromFile();
  }

  public void getDirectoryContent() {
    directoryContent.getDirectoryContent();
  }
}
