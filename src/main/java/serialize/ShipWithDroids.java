package serialize;


import java.io.Serializable;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ShipWithDroids implements Serializable {

  private String nameOfDroids;
  private String modelOfDroids;
  private transient int numberOfDroids;
  private int damageOfDroids;
  private static Logger logger = LogManager.getLogger();

  public String getNameOfDroids() {
    return nameOfDroids;
  }

  public void setNameOfDroids(String nameOfDroids) {
    this.nameOfDroids = nameOfDroids;
  }

  public String getModelOfDroids() {
    return modelOfDroids;
  }

  public void setModelOfDroids(String modelOfDroids) {
    this.modelOfDroids = modelOfDroids;
  }

  public int getNumberOfDroids() {
    return numberOfDroids;
  }

  public void setNumberOfDroids(int numberOfDroids) {
    this.numberOfDroids = numberOfDroids;
  }

  public int getDamageOfDroids() {
    return damageOfDroids;
  }

  public void setDamageOfDroids(int damageOfDroids) {
    this.damageOfDroids = damageOfDroids;
  }

  public void display() {
    logger.warn("Name of droid: " + nameOfDroids);
    logger.warn("Model of droid: " + modelOfDroids);
    logger.warn("Number of droids: " + numberOfDroids);
    logger.warn("Damage of droids: " + damageOfDroids);
  }
}
