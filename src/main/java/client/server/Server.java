package client.server;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.Iterator;
import java.util.Set;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


public class Server {

  private static Logger logger = LogManager.getLogger();
  private SelectionKey selectKy;
  private Selector selector;
  private ServerSocketChannel socket;
  private InetSocketAddress addr;
  private Set<SelectionKey> keys;
  private Iterator<SelectionKey> iterator;

  public Server() throws IOException {
    selector = Selector.open();
    socket = ServerSocketChannel.open();
    addr = new InetSocketAddress("localhost", 1111);
    socket.bind(addr);
    socket.configureBlocking(false);
    int ops = socket.validOps();
    selectKy = socket.register(selector, ops, null);
  }

  public void serverRun() throws IOException {

    while (true) {

      logger.warn("Waiting for new connection.");
      selector.select();

      keys = selector.selectedKeys();
      iterator = keys.iterator();

      while (iterator.hasNext()) {
        SelectionKey myKey = iterator.next();

        if (myKey.isAcceptable()) {
          SocketChannel client = socket.accept();
          client.configureBlocking(false);
          client.register(selector, SelectionKey.OP_READ);
          logger.warn("Connection Accepted: " + client.getLocalAddress() + "\n");

        } else if (myKey.isReadable()) {

          SocketChannel client = (SocketChannel) myKey.channel();
          ByteBuffer buffer = ByteBuffer.allocate(256);
          client.read(buffer);
          String result = new String(buffer.array()).trim();
          logger.warn("Message received: " + result);

          if (result.equals("q")) {
            client.close();
            logger.warn("Closing connection.");
            logger.warn(
                "Server will keep running. Try running client again to establish new connection");
          }
        }
        iterator.remove();
      }
    }

  }

}