package client.server;

import java.io.IOException;

public class ServerProgram {

  public static void main(String[] args) throws IOException {
    Server server = new Server();
    server.serverRun();
  }
}
