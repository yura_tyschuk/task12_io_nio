package client.server;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.util.ArrayList;
import java.util.Scanner;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Client {

  private static Logger logger = LogManager.getLogger();
  private static Scanner scanner = new Scanner(System.in);
  private static String s = " ";
  private InetSocketAddress addr;
  private SocketChannel client;
  private ArrayList<String> message;

  public Client() {
    message = new ArrayList<>();
  }

  private void setMessage() {
    while (true) {
      logger.fatal("Print messages: ");
      s = scanner.next();
      message.add(s);
      if (s.equals("q")) {
        break;
      }
    }
  }

  public void clientRun() throws IOException, InterruptedException {
    addr = new InetSocketAddress("localhost", 1111);
    client = SocketChannel.open(addr);

    logger.fatal("Connecting to Server on port 1111.");
    logger.fatal("If you want exit print 'q'");
    setMessage();

    for (String m : message) {

      byte[] message = new String(m).getBytes();
      ByteBuffer buffer = ByteBuffer.wrap(message);
      client.write(buffer);
      if (!m.equals("q")) {
        logger.fatal("Sending: " + m);
        buffer.clear();
      }

      Thread.sleep(2000);
    }
    client.close();
  }


}