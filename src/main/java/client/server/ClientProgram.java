package client.server;

import java.io.IOException;

public class ClientProgram {

  public static void main(String[] args) throws IOException, InterruptedException {
    Client client = new Client();
    client.clientRun();
  }
}
