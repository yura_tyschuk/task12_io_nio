package compare.perfomance;

import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.EOFException;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class CompareClass {

  private static Logger logger = LogManager.getLogger();
  private int count;
  private long time;

  public void usualReader() throws IOException {
    count = 0;
    time = System.currentTimeMillis();
    logger.warn("Usual read");
    InputStream inputStream = new FileInputStream("A17_FlightPlan.pdf");
    int data = inputStream.read();
    while (data != -1) {
      data = inputStream.read();
      count++;
    }
    inputStream.close();
    logger.fatal("Count: " + count);
    logger.fatal("Time: " + (System.currentTimeMillis() - time));
  }

  public void bufferedReader() throws IOException {
    count = 0;
    time = System.currentTimeMillis();
    logger.warn("Buffered read: ");
    DataInputStream in = new DataInputStream(
        new BufferedInputStream(
            new FileInputStream("A17_FlightPlan.pdf")));
    try {
      while (true) {
        byte b = in.readByte();
        count++;
      }

    } catch (EOFException e) {
    }
    in.close();
    logger.fatal("Count: " + count);
    logger.fatal("Time: " + (System.currentTimeMillis() - time));
  }

  public void bufferedReaderWithDifferentSizes(int bufferSize) throws IOException {
    count = 0;
    time = System.currentTimeMillis();
    logger.warn("Buffer size: " + bufferSize);
    DataInputStream in = new DataInputStream(
        new BufferedInputStream(
            new FileInputStream("A17_FlightPlan.pdf")));
    try {
      while (true) {
        byte b = in.readByte();
        count++;
      }
    } catch (IOException e) {

    }
    in.close();
    logger.fatal("Count: " + count);
    logger.fatal("Time: " + (System.currentTimeMillis() - time));
  }
}